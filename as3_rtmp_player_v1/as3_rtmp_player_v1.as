/**
 * 最简单的基于ActionScript的RTMP播放器
 * Simplest AS3 RTMP Player
 *
 * 朱世奇 Zhu Shiqi
 * hiccupzhu@163.com
 * http://blog.csdn.net/hiccupzhu
 * 
 * 本程序使用ActionScript3语言完成，播放RTMP服务器上的流媒体
 * 是最简单的基于ActionScript3的播放器。
 *
 * This software is written in Actionscript3, it plays stream
 * on RTMP server
 * It's the simplest RTMP player based on ActionScript3.
 * 
 */
package {
	import flash.display.Sprite;
	import flash.events.AsyncErrorEvent;
	import flash.events.MouseEvent;
	import flash.events.NetStatusEvent;
	import flash.external.ExternalInterface;
	import flash.media.Video;
	import flash.net.NetConnection;
	import flash.net.NetStream;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFieldType;
	import flash.utils.clearInterval;
	import flash.utils.setInterval;
	import flash.system.Security;
	
	import fl.controls.Button;
	import fl.controls.Label;
	
	public class as3_rtmp_player_v1 extends Sprite
	{
		public var nc:NetConnection;
		public var netStreamObj:NetStream;
		public var video:Video;
		public var playButton:Button;
		public var urlText:TextField;
		public var bufText:TextField;
		public var stream_id:String;
		public var status:Label;
		
		public var intervalID:uint;
		public var counter:int;
		
		public function as3_rtmp_player_v1()
		{
			Security.allowDomain("*");
			Security.allowInsecureDomain("*");
			
			
			playButton = new Button();
			playButton.x=stage.stageWidth-playButton.width;
			playButton.y=stage.stageHeight-playButton.height;
			playButton.emphasized=false;//按钮有边框
			playButton.toggle=false;//按钮按下之后不再弹起，再按一下才会弹起
			playButton.label="播放";
			playButton.addEventListener(MouseEvent.CLICK,onPlayClick);
			
			
			urlText = new TextField();
			with(urlText){
				type = TextFieldType.INPUT;
				x=1; 
				y=1;
				width=370;
				border=true;
				multiline=false;
				wordWrap=false;
			}
			urlText.x = 200;
			urlText.y=stage.stageHeight-playButton.height;
			urlText.height = 20;
			urlText.text = "rtmp://106.39.160.33/master/cctv1";
			
			
			bufText = new TextField();
			with(bufText){
				type = TextFieldType.INPUT;
				x=1; 
				y=1;
				width=20;
				border=true;
				multiline=false;
				wordWrap=false;
			}
			bufText.x = 575;
			bufText.y=stage.stageHeight-playButton.height;
			bufText.height = 20;
			bufText.text = "1";
			bufText.restrict = "0-9";
			
			
			status = new Label();
			with (status) { 
				autoSize=TextFieldAutoSize.LEFT;//标签文字显示的位置，左对齐、居中、右对齐  
				condenseWhite=false;//false保留多余的空格，HTML中才会用到  
				htmlText="";//以HTML方式显示的文字  
				selectable=true;//true鼠标可以选择文字  
				wordWrap=false;//false显示一行，true文字多就显示成多行  
				text="状态:";//标签的内容  
			}
			status.x = 1;
			status.y=stage.stageHeight-playButton.height;
			
			addChild(status);
			addChild(urlText);
			addChild(bufText);
			addChild(playButton);
		}
		
		function onPlayClick(e:MouseEvent){
			if(playButton.label == "播放"){
				var murl:String;
				var marr = urlText.text.split("/");
				
				trace("marr:"+marr);
				
				if(marr.length >= 5 && marr[4] != ""){
					stream_id = marr[4];
					var text:String = urlText.text;
					murl = text.substring(0, text.indexOf(stream_id) - 1);
				}else{
					murl = urlText.text;
				}
				
				trace("connect:"+murl+"  stream_id:"+stream_id);
				
				video = new Video();
				nc = new NetConnection();
				nc.addEventListener(NetStatusEvent.NET_STATUS, onConnectionStatus);
				nc.addEventListener(AsyncErrorEvent.ASYNC_ERROR, asyncErrorHandler);
				nc.connect(murl);
				
				playButton.label="停止";
			}else{
				if(netStreamObj)
					netStreamObj.pause();
				
				removeChild(video);
				clearInterval(intervalID);
				
				netStreamObj.close();
				nc.close();
				video.clear();
				
				nc = null;
				netStreamObj = null;
				video = null;
				
				playButton.label="播放";
			}
		}
		
		// play a recorded stream on the server
		private function doVideo(nc:NetConnection):void {
			trace("Creating NetStream");
			netStreamObj = new NetStream(nc);
			netStreamObj.client = {onMetaData:onMetaData, onPlayStatus :onPlayStatus};
			netStreamObj.bufferTime = int(bufText.text);
			
			netStreamObj.addEventListener(NetStatusEvent.NET_STATUS, onConnectionStatus);
			
			video.attachNetStream(netStreamObj);
			
			netStreamObj.play(stream_id);
			
			
			addChild(video);
			
			intervalID = setInterval(playback, 1000);
		}
		
		private function playback():void{
			trace((++counter) + " data: " + netStreamObj.bufferTime);
			var fps:int = int(netStreamObj.currentFPS + 0.5);
			status.text = "状态 FPS:"+fps+" Time:"+netStreamObj.time+" Buf:"+netStreamObj.bufferLength+"";
		}
		
		function onMetaData(data:Object):void{
			var _stageW:int = stage.stageWidth;
			var _stageH:int = stage.stageHeight - (playButton.height*2 + 5);
			
			var _videoW:int;
			var _videoH:int;
			var _aspectH:int; 
			
			var Aspect_num:Number; //should be an "int" but that gives blank picture with sound
			Aspect_num = data.width / data.height;
			
			//Aspect ratio calculated here..
			_videoW = _stageW;
			_videoH = _videoW / Aspect_num;
			_aspectH = (_stageH - _videoH) / 2;
			
			video.x = 0;
			video.y = _aspectH;
			video.width = _videoW;
			video.height = _videoH;
		}
		
		
		private function onConnectionStatus(event:NetStatusEvent):void
		{
			trace("event.info.level: " + event.info.level + "\n", "event.info.code: " + event.info.code);
			switch (event.info.code)
			{
				case "NetConnection.Connect.Success":
					doVideo(nc);
					break;
				case "NetConnection.Connect.Failed":
					break;
				case "NetConnection.Connect.Rejected":
					break;
				case "NetStream.Play.Stop":
					break;
				case "NetStream.Play.StreamNotFound":
					break;
			}
		}
		
		
		public function asyncErrorHandler(event:AsyncErrorEvent):void{
			trace("asyncErrorHandler.." + "\r");
		}
		
		
		
		function onPlayStatus(o:Object):void{
			trace(o);
		}
		
		
	}
}
