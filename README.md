##FlashMedia Player


### 概述

本项目主要使用ActionScript3的方式调用Adobe Flash播放器来实现RTMP流媒体的播放；


### 编译环境

1. Adobe Flash Builder 4.7 Premium
2. Adobe Flash Professional CS6: Version 12.0.0.481


### 已编译包

1. 简单RTMP Flash Player【[下载](http://pan.baidu.com/s/1i45l8Z7)】 提取密码：983u
  
2. 简单基于浏览器的player 【[下载](http://pan.baidu.com/s/1bo7zHzL)】 提取密码：vrgk



### 联系我

EMail：hiccupzhu@163.com