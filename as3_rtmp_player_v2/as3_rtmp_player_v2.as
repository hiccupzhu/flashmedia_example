/**
 * 最简单的基于ActionScript的RTMP播放器
 * Simplest AS3 RTMP Player
 *
 * 朱世奇 Zhu Shiqi
 * hiccupzhu@163.com
 * http://blog.csdn.net/hiccupzhu
 * 
 * 本程序使用ActionScript3语言完成，播放RTMP服务器上的流媒体
 * 是最简单的基于ActionScript3的播放器。
 *
 * This software is written in Actionscript3, it plays stream
 * on RTMP server
 * It's the simplest RTMP player based on ActionScript3.
 * 
 */
package {
	import flash.display.Sprite;
	import flash.events.AsyncErrorEvent;
	import flash.events.MouseEvent;
	import flash.events.NetStatusEvent;
	import flash.external.ExternalInterface;
	import flash.media.Video;
	import flash.net.NetConnection;
	import flash.net.NetStream;
	import flash.system.Security;
	import flash.text.TextField;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFieldType;
	
	import fl.controls.TextInput;
	
	public class as3_rtmp_player_v2 extends RtmpPlayer
	{
		var inText:TextInput;
		
		public function as3_rtmp_player_v2()
		{
			Security.allowDomain("*");
			Security.allowInsecureDomain("*");
			
			
			if(ExternalInterface.available) {
				ExternalInterface.addCallback('play', onPlayFunc);
				ExternalInterface.addCallback('stop', onStopFunc);
			}
			
//			playURL('rtmp://106.39.160.33/master/cctv1');
			
		}
		
		
		function onPlayFunc(murl:String):void{
			playURL(murl);
		}
		
		function onStopFunc():void{
			stop();
		}
	}
}
