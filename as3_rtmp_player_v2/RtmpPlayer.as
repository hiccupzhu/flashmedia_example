package
{
	import flash.display.Sprite;
	import flash.events.AsyncErrorEvent;
	import flash.events.MouseEvent;
	import flash.events.NetStatusEvent;
	import flash.external.ExternalInterface;
	import flash.media.Video;
	import flash.net.NetConnection;
	import flash.net.NetStream;
	import flash.net.drm.AddToDeviceGroupSetting;
	import flash.text.TextFieldAutoSize;
	import flash.text.TextFieldType;
	import flash.utils.clearInterval;
	import flash.utils.setInterval;
	import flash.filters.BlurFilter;
	import flash.filters.BitmapFilterQuality;
	import flash.text.TextFormat;
	
	import fl.controls.Label;

	public class RtmpPlayer extends Sprite
	{
		protected var _netConnect:NetConnection;
		protected var _netStreamObj:NetStream;
		protected var _video:Video;
		protected var _stream_id:String;
		public var _status:Label;
		public var _intervalID:uint;
		private var _circleMask:Sprite;
		
		public function RtmpPlayer()
		{
			
			
			_status = new Label();
			with (_status) { 
				autoSize=TextFieldAutoSize.LEFT;//标签文字显示的位置，左对齐、居中、右对齐  
				condenseWhite=false;//false保留多余的空格，HTML中才会用到  
				htmlText="";//以HTML方式显示的文字  
				selectable=true;//true鼠标可以选择文字  
				wordWrap=false;//false显示一行，true文字多就显示成多行  
				text="状态:";//标签的内容  
				height=50;
				z=1;
			}
			_status.x = 0;
			_status.y = 0;
			
			var tf:TextFormat=new TextFormat ("微软雅黑",14,0xFFFFFF);
			_status.setStyle("textFormat",tf);
			
//			_circleMask = new Sprite();
//			_circleMask.graphics.beginFill(0xff0000);
//			_circleMask.graphics.drawCircle(60,60,60);
//			_circleMask.graphics.endFill();
//			_circleMask.z = 200;
//			
//			//用滤镜模糊化
//			_circleMask.filters = [new BlurFilter(20, 20, BitmapFilterQuality.HIGH)];
//			_circleMask.cacheAsBitmap = true;
//			
//			addChild(_circleMask);    
//			addChild(_status);
		}
		
		
		public function playURL(murl:String):void
		{
			var app_url:String;
			var streamid:String;
			var marr = murl.split("/");
			
			trace("marr:"+marr);
			
			if(marr.length >= 5 && marr[4] != ""){
				streamid = marr[4];
				app_url = murl.substring(0, murl.indexOf(streamid) - 1);
			}else{
				murl = murl;
			}
			
			play(app_url, streamid);
		}
		
		public function play(app_url:String, streamid:String):void
		{
			if(_netConnect)
			{
				stop();
			}
				
			_video = new Video();
			
			_netConnect = new NetConnection();
			_netConnect.addEventListener(NetStatusEvent.NET_STATUS, onConnectionStatus);
			_netConnect.addEventListener(AsyncErrorEvent.ASYNC_ERROR, asyncErrorHandler);
			
			_stream_id = streamid;
			_netConnect.connect(app_url);
			
			_intervalID = setInterval(update_status, 1000);
		}
		
		public function stop() {
			removeChild(_video);
			removeChild(_status);
			
			clearInterval(_intervalID);
			_netStreamObj.close();
			_netConnect.close();
			_video.clear();
			
			_netStreamObj = null;
			_netConnect = null;
			_video = null;
		}
		
		
		public function onConnectionStatus(event:NetStatusEvent):void
		{
			trace("event.info.level: " + event.info.level + "\n", "event.info.code: " + event.info.code);
			switch (event.info.code)
			{
				case "NetConnection.Connect.Success":
				{
					trace("Creating NetStream");
					_netStreamObj = new NetStream(_netConnect);
					_netStreamObj.client = {onMetaData:onMetaData, onPlayStatus :onPlayStatus};
					_netStreamObj.bufferTime = 1;
					
					_netStreamObj.addEventListener(NetStatusEvent.NET_STATUS, onConnectionStatus);
					
					_video.attachNetStream(_netStreamObj);
					
					_netStreamObj.play(_stream_id);
					
					
					
					addChild(_video);
					addChild(_status);
				
				}
					break;
				case "NetConnection.Connect.Failed":
					break;
				case "NetConnection.Connect.Rejected":
					break;
				case "NetStream.Play.Stop":
					break;
				case "NetStream.Play.StreamNotFound":
					break;
			}
		}
		
		
		public function onMetaData(data:Object):void{
			var _stageW:int = stage.stageWidth;
			var _stageH:int = stage.stageHeight;
			
			var _videoW:int;
			var _videoH:int;
			var _aspectH:int; 
			
			var Aspect_num:Number; //should be an "int" but that gives blank picture with sound
			Aspect_num = data.width / data.height;
			
			//Aspect ratio calculated here..
			_videoW = _stageW;
			_videoH = _videoW / Aspect_num;
			_aspectH = (_stageH - _videoH) / 2;
			
			_video.x = 0;
			_video.y = _aspectH;
			_video.width = _videoW;
			_video.height = _videoH;
		}
		
		private function update_status():void{
			var fps:int = int(_netStreamObj.currentFPS + 0.5);
			_status.text = "状态 FPS:"+fps+" Time:"+_netStreamObj.time+" Buf:"+_netStreamObj.bufferLength+"";
		}
		
		public function asyncErrorHandler(event:AsyncErrorEvent):void{
			trace("asyncErrorHandler.." + "\r");
		}
		
		public function onPlayStatus(o:Object):void{
			trace(o);
		}
	}
}